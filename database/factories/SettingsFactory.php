<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Setting;
use Faker\Generator as Faker;

$factory->define(Setting::class, function (Faker $faker) {
    return [
        //
        "card_1" => $faker->name,
        "card_2" => $faker->name,
        "card_3" => $faker->name,
        "card_4" => $faker->name,
        "user_id" => factory(\App\User::class),
    ];
});
