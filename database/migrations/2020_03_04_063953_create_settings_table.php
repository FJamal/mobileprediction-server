<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string("card_1");
            $table->string("card_2");
            $table->string("card_3");
            $table->string("card_4");
            $table->boolean("app_status")->default(false);
            $table->unsignedBigInteger("user_id");
            $table->timestamps();

            $table->foreign("user_id")
              ->references("id")
              ->on("users")
              ->onDelete("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
