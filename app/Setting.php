<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $fillable = [
      "card_1",
      "card_2",
      "card_3",
      "card_4",
      "app_status",
      "user_id"
    ];

    protected $hidden = [
      "user_id",
      "id",
    ];

    public function user()
    {
      return $this->belongsTo(User::class);
    }
}
