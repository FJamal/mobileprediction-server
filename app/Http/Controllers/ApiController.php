<?php

namespace App\Http\Controllers;

//TEST
use Illuminate\Support\Facades\Validator;

use Illuminate\Http\Request;
use App\User;
use App\Setting;

class ApiController extends Controller
{
    public function test(Request $request)
    {
      // $test = $request->json()->all();
      // return ["test" => $test];

      //return $request->json()->all();

      //$data = $request->json()->all();
      //$email = $data->emailid;
      //return ["test" => "hello"];



       //$postInput = file_get_contents('php://input');
       //$data = json_decode($postInput, true);
       return $request->json()->all();

    }


    public function register(Request $request)
    {
        $test = $request->json()->all();
        //return ["test" => $test];
        $postInputs = file_get_contents("php://input");
        $data = json_decode($postInputs, true);

        foreach ($data as $key => $value)
        {
          $request[$key] = $data[$key];
        }

        //return $test;

        // $validator = $request->validate([
        //   'name' => ['required', 'string', 'max:255'],
        //   'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
        //   'password' => ['required', 'string', 'min:8', 'confirmed'],
        // ]);


        $validator = Validator::make($test, [
          'name' => ['required', 'string', 'max:255'],
          'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
          'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);

        //return ["test" => $test["email"]];
        if($validator->fails())
        {
          $errors = $validator->errors()->first();
          return ["serverErrorMessage" => $errors,
                  "serverError" => true];
        }

        //storing in DB
        $user = User::create([
          "name" => $test["name"],
          "email" => $test["email"],
          "password" => \Hash::make($test["password"]),
          "api_token" => \Str::random(80),
        ]);



        return ["serverError" => false,
                "api_token" => $user->api_token,
                "name" => $user->name,
                "url" => $this->userUrl($user->api_token),];
    }


    public function login(Request $request)
    {
      $data = $request->json()->all();

      //Validate the inputs
      $validator = Validator::make($data, [
        "email" => ["required" , "string", "email", "max:255"],
        "password" => ["required" , "string", "min:8"]
      ]);

      if($validator->fails())
      {
        $error = $validator->errors()->first();
        return["serverErrorMessage" => $error,
                "serverError" => true];
      }

      //find user in DB
      $user = User::where("email" , $data["email"])->first();

      if(!$user)
      {
        return["serverErrorMessage" => "No such user",
                "serverError" => true];
      }

      //match the passwords
      if(\Hash::check($data["password"], $user->password))
      {
        //user exists send to app the api token for the app
        return ["serverError" => false,
                "api_token" => $user->api_token,
                "name" => $user->name,
                "url" => $this->userUrl($user->api_token),
                "settings" => $user->setting];
      }

      return ["serverError" => true,
              "serverErrorMessage" => "Invalid email or password"];
    }


    /*Used to save settings for the user*/
    public function saveSettings(Request $request)
    {
        $input = $request->json()->all();

        //Validate the inputs
        $validator = Validator::make($input, [
          "card_1" => "required|max:255",
          "card_2" => "required|max:255",
          "card_3" => "required|max:255",
          "card_4" => "required|max:255",
        ]);

        //if validation fails
        if($validator->fails())
        {
          $error = $validator->errors()->first();
          return["serverErrorMessage" => $error,
                  "serverError" => true];
        }

        //find user
        $user = User::where("api_token", $input["api_token"])->first();
        if($user)
        {
          if($user->setting)
          {
            //Update the existing settings
            Setting::where("user_id", $user->id)->update([
              "card_1" => $input["card_1"],
              "card_2" => $input["card_2"],
              "card_3" => $input["card_3"],
              "card_4" => $input["card_4"],
            ]);

            return ["serverError" => false];
          }
          else
          {
            //Create a new settings for this user for the first time
            Setting::create([
              "card_1" => $input["card_1"],
              "card_2" => $input["card_2"],
              "card_3" => $input["card_3"],
              "card_4" => $input["card_4"],
              "user_id" => $user->id,
            ]);

            return ["serverError" => false];
          }
        }
    }

    //used to get the saved settings of the user
    public function getsettings(Request $request)
    {
      $input = $request->json()->all();

      //get user
      $user = User::where("api_token", $input["api_token"])->first();

      if($user)
      {
        $setting = Setting::where("user_id", $user->id)->first();
        if($setting)
        {
          //case where setting is already there in DB for the user
          return ["setting" => $setting,
                  "userName" => $user->name,
                  "url" => $this->userUrl($user->api_token)];
        }
        else
        {
          return ["userName" => $user->name];
        }


      }


    }




    protected function userUrl($api_token)
    {
      //get the user id
      $user = User::where("api_token", $api_token)->first();

      //creating a number based url with 403 as the first numbers
      $routename = "/403" . $user->id;
      $url = url("/");
      $fullUrl = $url . $routename;
      return $fullUrl;


    }
}
