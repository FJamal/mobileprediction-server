<?php
use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// Route::post("/user", function (Request $request) {
//   //return $request;
//   $input = $request->input("password");
//   return ["password" => $input];
// });


// Route::post("/user", 'ApiController@index');
//
// Route::get("/user", function (Request $request) {
//   return $request;
// });
