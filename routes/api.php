<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
//
// });

Route::get("/user", function (Request $request) {
  return $request;
});


Route::post("/login", "ApiController@login");

Route::post("/register", "ApiController@register");

//for saving settings
Route::post("/savesettings", "ApiController@saveSettings");


//to get settings of the user
Route::post("getdata", "ApiController@getsettings");

/*TEST*/
// Route::middleware("auth:api")->post("/savesettings", function (Request $request) {
//   return $request->user();
// });
